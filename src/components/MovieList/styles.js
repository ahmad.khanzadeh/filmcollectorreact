import { makeStyles } from "@mui/styles";

export default makeStyles((theme)=>({
    root:{
        display:'flex',
        height:'100%',
    },
    moviesContainer:{
        display:'flex',
        flexWrap:'wrap',
        justifyContant:'space-between',
        overflow:'auto',
        background: 'red',
        color:'green',
        [theme.breakpoints.down('sm')]:{
            justifyContant:'center',
        },
    }
}))

// bist 11.12 von 4