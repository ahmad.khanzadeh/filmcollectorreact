import { makeStyles } from '@mui/styles';

const drawerWidth=240;

export default makeStyles((theme)=>({
    toolbar:{
        height:'80px',
        display:'flex',
        justifyContent:'space-between',
        margin:'240px',
        [theme.breakpoints.down('sm')]: {
            marginLeft: 0,
            flexWrap: 'wrap',
          },
    },
    theAppBar:{
        //  height:'100px',
        // display:'flex',
        // justifyItems:'space-between'
        margin:'0 !important',
    },
    menuButton:{
        marginRight: theme.spacing(2),
        // these styles will show only in devices bigger then phones
        [theme.breakpoints.up('sm')]: {
            display: 'none',
          },
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width:drawerWidth,
            flexShrink: 0,
          },
    },
    drawerPaper:{
        width: drawerWidth,
    },
    linkButton:{
        '&hover':{
            color:'white !important',
            textDecoration:'none'
        }
    }
}))