import React from 'react';
import { CssBaseline } from '@mui/material';
import { Routes, Route } from 'react-router-dom';
import { Actors, MovieInformation, Movies, NavBar, Profile } from './index.js';
import useStyles from './styles';

const App = () =>{
  const classes=useStyles();
  return(
    <div className={classes.root}>
      <CssBaseline />
      <NavBar />
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Routes>
          <Route exact path="/" element={<h1> Home element</h1>} />
          <Route exact path="/movies" element={<MovieInformation />}>
            it is not displaying
          </Route>
          <Route exact path="/movies/:id" element={<Movies />} />
          <Route exact path="/actors/:id" element={<Actors />} />
          <Route exact path="/profile/:id" element={<Profile />} />
          <Route exact path="/info" element={<MovieInformation />} />
        </Routes>
      </main>
    </div>
  );
} 

export default App;
