import { configureStore } from '@reduxjs/toolkit';

import { tmdbApi } from '../services/TMDB';
import  genreOrCategoryReducer from '../featurs/currentGenreOrCategory';

export default configureStore({
    reducer:{
        [tmdbApi.reducerPath]: tmdbApi.reducer,
        currentGenreOrCategory: genreOrCategoryReducer,
    },
});